# hugo-minos-theme

a [Hugo](https://gohugo.io/) theme ported from  Hexo theme [Minos](https://github.com/ppoffice/hexo-theme-minos)

Forked from deanlee's [hugo-minos-theme](https://github.com/deanlee/hugo-minos-theme)
